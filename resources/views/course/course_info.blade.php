@extends('layout.app')

@section('title', 'Course Info')




@section('content')

	<div class = "row text-center  justify-content-center">
		<div class = "col-sm-6 ">
			<div class = "row ">
				<div class = "col-sm-3"><strong>Name : </strong></div>
				<div class = "col-sm-9 text-left">{{$course->course_name}}
					<hr>
				</div>


				<div class = "col-sm-3"><strong>Students : </strong></div>
				<div class = "col-sm-9 text-left">
					@foreach($course->student as $student)

						<p>- {{$student->name}}</p>
						<p>- {{$student->age}} years old</p>
						<hr>
					@endforeach
				</div>


			</div>
		</div>
	</div>

@endsection