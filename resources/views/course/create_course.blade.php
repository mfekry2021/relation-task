@extends('layout.app')

@section('title', 'Create Course')




@section('content')
	<h2 class="text-center">Create Course</h2><br>
	<div class = "row text-center  justify-content-center">

		<div class = "col-sm-6 ">
			<form method = "post" action = "/course/store">

				@csrf

				<div class = "form-group row {{$errors->has('name') ? ' has-error' : '' }}">
					<label for = "inputEmail3" class = "col-sm-2 col-form-label">  Name</label>
					<div class = "col-sm-10">
						<input type = "text" name = "name" value = "{{old('name')}}" class = "form-control"
						       id = "inputEmail3">

						@if ($errors->has('name'))
							<span class = "help-block">
                                             <strong>{{ $errors->first('name') }}</strong>
                                                  </span>
						@endif

					</div>
				</div>


				<div class = "form-group">
					<div>
						<button type = "submit" class = "btn btn-primary">Create</button>
					</div>
				</div>
			</form>

		</div>
	</div>

@endsection