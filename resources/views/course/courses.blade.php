@extends('layout.app')

@section('title', 'Courses')






@section('content')

	<!-- Display error and success message -->

	<div class = "box-header">
		@include('inc.messages')
	</div>
	<div class = "box-header">
		@include('inc.error')
	</div>

	<div class = "row text-center  justify-content-center">
		<div class = "col-sm-8 ">
			<table class = "table">
				<thead class = "thead-dark">
				<tr>
					<th scope = "col">ID</th>
					<th scope = "col">Course</th>
					<th scope = "col">controle</th>
				</tr>
				</thead>
				<tbody>
				@foreach($courses as $course)
					<tr>
						<th scope = "row">{{$course->id}}</th>
						<td>{{$course->course_name}}</td>
						<td><a href = "/course/{{$course->id}}/show" class = "btn btn-info">Show</a></td>
					</tr>

				@endforeach
				</tbody>
			</table>
		</div>
	</div>


@endsection