@extends('layout.app')

@section('title', 'Students')




@section('content')

	<!-- Display error and success message -->

	<div class="box-header">
		@include('inc.messages')
	</div>
	<div class="box-header">
		@include('inc.error')
	</div>

	<div class = "row text-center  justify-content-center">
		<div class = "col-sm-8 ">
			<table class = "table">
				<thead class = "thead-dark">
				<tr>
					<th scope = "col">ID</th>
					<th scope = "col">Student Name</th>
					<th scope = "col">Age</th>
					<th scope = "col">controle</th>
				</tr>
				</thead>
				<tbody>
				@foreach($students as $student)
					<tr>
						<th scope = "row">{{$student->id}}</th>
						<td>{{$student->name}}</td>
						<td>{{$student->age}}</td>
						<td><a href = "/student/{{$student->id}}/show" class = "btn btn-info">Show</a></td>
					</tr>

				@endforeach
				</tbody>
			</table>
		</div>
	</div>

@endsection