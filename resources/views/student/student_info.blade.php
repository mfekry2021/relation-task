@extends('layout.app')

@section('title', 'Student Info')




@section('content')

	<div class = "row text-center  justify-content-center">
		<div class = "col-sm-6 ">
			<div class = "row ">
				<div class = "col-sm-3"><strong>Name : </strong></div>
				<div class = "col-sm-9 text-left">{{$student->name}}<hr></div>

				<div class = "col-sm-3"><strong>Age : </strong></div>
				<div class = "col-sm-9 text-left">{{$student->age}}<hr></div>

				<div class = "col-sm-3"><strong>City : </strong></div>
				<div class = "col-sm-9 text-left">{{$student->city->city_name}}<hr></div>

				<div class = "col-sm-3"><strong>Address : </strong></div>
				<div class = "col-sm-9 text-left">{{$student->address->address}}<hr></div>

				<div class = "col-sm-3"><strong>Courses : </strong></div>
				<div class = "col-sm-9 text-left">
					@foreach($student->course as $course)
						<p>- {{$course->course_name}}</p>
					@endforeach
				</div>


			</div>
		</div>
	</div>

@endsection