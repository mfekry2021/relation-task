@extends('layout.app')

@section('title', 'Create Student')




@section('content')
	<h2 class="text-center">Create Student</h2><br>

	<div class = "row text-center  justify-content-center">
		<div class = "col-sm-6 ">
			<form method="post" action="/student/store">

				@csrf

				<div class = "form-group row {{$errors->has('name') ? ' has-error' : '' }}">
					<label for = "inputEmail3" class = "col-sm-2 col-form-label">Name</label>
					<div class = "col-sm-10">
						<input type = "text" name = "name" value = "{{old('name')}}" class = "form-control"
						       id = "inputEmail3">

						@if ($errors->has('name'))
							<span class = "help-block">
                                             <strong>{{ $errors->first('name') }}</strong>
                                                  </span>
						@endif

					</div>
				</div>
				<div class = "form-group row {{$errors->has('age') ? ' has-error' : '' }}">
					<label for = "inputPassword3" class = "col-sm-2 col-form-label">Age</label>
					<div class = "col-sm-10">
						<input type = "number" name = "age" value = "{{old('age')}}" class = "form-control"
						       id = "inputPassword3">

						@if ($errors->has('age'))
							<span class = "help-block">
                                             <strong>{{ $errors->first('age') }}</strong>
                                                  </span>
						@endif

					</div>
				</div>

				<div class = "form-group row {{$errors->has('address') ? ' has-error' : '' }}">
					<label for = "inputPassword3" class = "col-sm-2 col-form-label">Address</label>
					<div class = "col-sm-10">
						<input type = "text" name = "address" value = "{{old('address')}}" class = "form-control"
						       id = "inputPassword3">

						@if ($errors->has('address'))
							<span class = "help-block">
                                             <strong>{{ $errors->first('address') }}</strong>
                                                  </span>
						@endif
					</div>
				</div>

				<div class = "form-group row {{$errors->has('city') ? ' has-error' : '' }}">
					<label class = "col-sm-2 col-form-label">City</label>
					<div class = "col-sm-10">
						<select class = "custom-select" name = "city">
							<option value = "" disabled hidden></option>

							@foreach($cities as $city)

								<option value = "{{$city->id}}">{{$city->city_name}}</option>

							@endforeach
						</select>

						@if ($errors->has('city'))
							<span class = "help-block">
                                             <strong>{{ $errors->first('city') }}</strong>
                                                  </span>
						@endif
					</div>
				</div>


				<div class = "form-group">
					<div>
						<button type = "submit" class = "btn btn-primary">Create</button>
					</div>
				</div>
			</form>

		</div>
	</div>

@endsection