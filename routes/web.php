<?php

	/*
	|--------------------------------------------------------------------------
	| Web Routes
	|--------------------------------------------------------------------------
	|
	| Here is where you can register web routes for your application. These
	| routes are loaded by the RouteServiceProvider within a group which
	| contains the "web" middleware group. Now create something great!
	|
	*/

	Route::get( '/', function ()
	{
		return view( 'welcome' );
	} );


	/********** Students **********/

	// show all students

	Route::get( '/students', 'StudentController@index' );

	// get student details

	Route::get( '/student/{id}/show', 'StudentController@show' );

	// create new student

	Route::get( '/student/create', 'StudentController@create' );
	Route::post( '/student/store', 'StudentController@store' );


	/********** Courses **********/

	// get all courses

	Route::get( '/courses', 'CourseController@index' );

	// get course details

	Route::get( '/course/{id}/show', 'CourseController@show' );

	// create new course

	Route::get( '/course/create', 'CourseController@create' );
	Route::post( '/course/store', 'CourseController@store' );
