<?php

	namespace App\Http\Controllers;

	use App\Course;
	use Illuminate\Http\Request;

	class CourseController extends Controller
	{
		/****** get all courses ******/

		public function index()
		{

			$courses = Course::select( 'id', 'course_name' )->get();

			return view( 'course.courses', compact( 'courses' ) );
		}


		/****** get course details ******/

		public function show( $id )
		{

			// get student data

			$course = Course::with( 'student' )->find( $id );

			return view( 'course.course_info', compact( 'course' ) );
		}

		/****** create course ******/

		public function create()
		{

			return view( 'course.create_course' );
		}

		/******Store course******/

		public function store( Request $request )
		{

			//make validation

			$this->validate( $request, [
				'name' => 'required|alpha_spaces',
			] );

			Course::create( ['course_name' => $request->name] );


			return redirect( '/courses' )->with( 'success', 'Course added succfully' );
		}
	}
