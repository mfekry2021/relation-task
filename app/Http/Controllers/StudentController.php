<?php

	namespace App\Http\Controllers;

	use App\Address;
	use App\City;
	use App\Student;
	use Carbon\Carbon;
	use Illuminate\Http\Request;

	class StudentController extends Controller
	{

		/****** get all students ******/

		public function index()
		{

			$students = Student::select( 'id', 'name', 'age' )->get();

			return view( 'student.students', compact( 'students' ) );

		}


		/****** get student details ******/

		public function show( $id )
		{

			// get student data

			$student = Student::with( 'city', 'address', 'course' )->find( $id );


			return view( 'student.student_info', compact( 'student' ) );
		}

		/****** create student ******/

		public function create()
		{

			// get cities

			$cities = City::select( 'id', 'city_name' )->get();


			return view( 'student.create_student', compact( 'cities' ) );
		}

		/******Store student******/

		public function store( Request $request )
		{

			//make validation

			$this->validate( $request, [
				'name'    => 'required|alpha_spaces',
				'age'     => 'required|numeric',
				'city'    => 'required|exists:cities,id',
				'address' => 'required|string',
			] );

			$student = Student::create( [
				                            'name'    => $request->name,
				                            'age'     => $request->age,
				                            'city_id' => $request->city
			                            ] );

			Address::create( [
				                 'address'    => $request->address,
				                 'student_id' => $student->id,
				                 'created_at' => Carbon::now()
			                 ] );


			return redirect( '/students' )->with( 'success', 'student added succfully' );
		}
	}
