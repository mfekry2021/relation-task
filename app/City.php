<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
	protected $guarded = [];

	// relation with city

	public function student()
	{

		return $this->belongsTo( 'App\Student' );
	}
}
